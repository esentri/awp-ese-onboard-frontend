import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from "./pages/home/Home";
import Login from "./pages/login/Login"
import Kreis from "./pages/kreis/Kreis"
import New from "./pages/new/New"
import Profil from "./pages/profil/Profil"
import Single from "./pages/single/Single"
import Startpaket from "./pages/startpaket/Startpaket"
import ErsterTag from "./pages/tag/Tag"
import Esentri from "./pages/esentri/Esentri"

import { 
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";


function App() {
  return(
    <div className="App">
      
  <BrowserRouter>
    <Routes>
      <Route path="/">
        <Route index element={<Login />} />
        <Route path="home" element={<Home />} />
        <Route path="kreis" element={<Kreis />} />
        <Route path="login" element={<Login />} />
        <Route path="new" element={<New />} />
        <Route path="profil" element={<Profil />} />
        <Route path="single" element={<Single />} />
        <Route path="startpaket" element={<Startpaket />} />
        <Route path="tag" element={<ErsterTag />} />
        <Route path="esentri" element={<Esentri />} />
      </Route>
    </Routes>
  </BrowserRouter>
  
    </div>

  )
}

export default App;
