import "./sidebar.scss"
import esentriLogo from "../../resources/esentri_grau-gruen.png"
import PresentIcon from "../../resources/presentbig.png"
import esentri_e from "../../resources/esentribig.png"
import patrick from "../../resources/Patrick.png"
import LanguageIcon from '@mui/icons-material/Language';
import PeopleIcon from '@mui/icons-material/People';
import RocketLaunchIcon from '@mui/icons-material/RocketLaunch';
import PersonIcon from '@mui/icons-material/Person';
import { Link } from "react-router-dom"
import LogoutButton from "../../resources/logout.png"
import React, {useState} from 'react'


const Logout = () => {
  const [user, setUser] = useState({name: "", email: ""});
  setUser ({ name: "", email: ""});
}

const Sidebar = () => {
  return (
    <div className="sidebar">
      <div className="top">
        <Link to="/home">
        <img src={esentriLogo} alt="Logo" className="logo1" />
        </Link>
        </div>
        <hr />

      
        <div className="center">
          <div className="profil">
            <Link to="/profil" style={{textDecoration:"none"}}>
            <img src={patrick} alt="Profilbild" className="profilbild" />
            </Link>
            <nobr><b>Patrick Schepanek</b></nobr>
            <p>patrick.schepanek@esentri.de</p>
            
            </div>
            <ul className="navigation">
            <Link to="/home" style={{textDecoration:"none"}}>
            <li>
                <LanguageIcon style={{ color: 'black' }} />
              <span>Übersicht</span>
            </li>
            </Link>
            <Link to="/kreis" style={{textDecoration:"none"}}>
            <li>
              <PeopleIcon color="primary" style={{ color: 'black' }}/>
              <span>Mein Kreis</span>
            </li>
            </Link>
            <Link to="/tag" style={{textDecoration:"none"}}>
            <li>
            <RocketLaunchIcon color="primary" style={{ color: 'black' }}/>
              <span>Mein erster Tag</span>
            </li>
            </Link>
            <Link to="/startpaket" style={{textDecoration:"none"}}>
            <li>
            <img src={PresentIcon} height={24} width={24} alt="Present" className="present1" style={{ color: 'black' }} />
              <span>Mein Startpaket</span>
            </li>
            </Link>
            {/* <Link to="/esentri" style={{textDecoration:"none"}}>
            <li>
            <img src={esentri_e} height={24} width={24} alt="esentrie" className="esentrie" style={{ color: 'black' }} />
              <span>Über esentri</span>
            </li> -->
            </Link>
            <Link to="/profil" style={{textDecoration:"none"}}>
            <li>
              <PersonIcon color="primary" style={{ color: 'black' }} />
              <span>Mein Profil</span>
            </li>
            </Link>*/}
            <Link to="/login" style={{textDecoration:"none"}}>
            <li>
              <img src={LogoutButton} height={24} width={24} className="" style={{ color: 'black' }} />
              <span>Logout</span>
            </li>
            


            </Link>

            </ul>
            </div>
    
    </div>
  );
};

export default Sidebar