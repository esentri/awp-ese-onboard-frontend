import React from 'react'
import "./startpaket.scss"
import airpods from "../../resources/AirPods.png"
import bildschirm from "../../resources/15gut.png"
import { Link } from "react-router-dom"

const Startpaket = ({ type }) => {
  let data;

  switch(type){
    case "produkt1":
      data={
       img1: <img src={airpods} alt="Logo" height="175px" />,
      };
      break;

    case "produkt2":
      data={
        img1:  <Link style={{textDecoration:"none"}} to="/startpaket"><div className='mitte'><p >+</p> <p>Produkt hinzufügen</p> </div></Link>,
      };
      break;

    case "produkt3":
      data={
        img1: <img src={bildschirm} alt="plapla" height="175px" />,
      };
      break;

      default:
      break;

 
  }
  return (
    <div className='widgets'>
        <div className='startpaket'>
              <div className='bild'>{data.img1}</div>
            </div>
        </div>

  )
}

export default Startpaket