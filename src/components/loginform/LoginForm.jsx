import React, {useState} from 'react'
import { Link, useHref } from 'react-router-dom';

function LoginForm ({Login, error}) {

    const adminUser = {
        name: "Patrick Schepanek",
        password: "admin123"
      }

    const [details, setDetails] = useState({name: "", email: "", password: ""});

    const submitHandler = e => {
        e.preventDefault();

        Login(details);
    }

    if(details.name == adminUser.name && details.password == adminUser.password){
        return (
            <form onSubmit={submitHandler}>
                <div className="form-inner">
                    <h2>Onboarding</h2>
                    {(error != "") ? (<div className='error'>{error}</div>) : ""}
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" name="name" id="name" onChange={e => setDetails({...details, name: e.target.value})} value={details.name} />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="password">Passwort:</label>
                        <input type="password" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value={details.password} />
                    </div>
                    <Link to="/home" >
                    <input type ="submit" value= "LOGIN" />
                    </Link>
                </div>

            </form>
        )
    }
    else{
        return (
            <form onSubmit={submitHandler}>
                <div className="form-inner">
                    <h2>Onboarding</h2>
                    {(error != "") ? (<div className='error'>{error}</div>) : ""}
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                        <input type="text" name="name" id="name" onChange={e => setDetails({...details, name: e.target.value})} value={details.name} />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="password">Passwort:</label>
                        <input type="password" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value={details.password} />
                    </div>
                    <input type ="submit" value= "LOGIN" />
                </div>

            </form>
        )
    }

}

export default LoginForm;