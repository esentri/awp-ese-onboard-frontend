import React from 'react'
import "./meinkreis.scss"
import { useState } from "react";

function Meinkreis() {
  const [readMore,setReadMore]=useState(false);
  const extraContent=<div>
    <p className="inhalt">

              <br/> Niemand möchte für jedes Problem das Rad neu erfinden. Dennoch erreicht man bei einer gewissen fachlichen oder
              <br/> technischen Tiefe in der Regel den Punkt, an dem Handarbeiten gefragt sind. 
              <br/> 
              <br/> In der Service Forge machen wir diese Individualität zu Ihrer Stärke und weil es Einzigartigkeit nicht von der Stange gibt, setzen wir die richtigen
              <br/> Werkzeuge ein, um in einem definierten Rahmen von Technologien die passende, individuelle Lösung zu schaffen. 
              <br/> 
              <br/> Wir verstehen Softwareentwicklung als Handwerkskunst.
              <br/> Unsere Experten sind Pioniere dieser Kunst, die Kreativität, Erfahrung, Intuition und Wissen 
              <br/> zusammenbringt. 
              <br/> 
              <br /> <span style={{fontWeight: 'bold'}}>Zweck und Vision: </span> 
              <br/> Die Service Forge ist esentris BackBone und nicht nur Service, sondern auch Kaderschmiede.
              <br/> Wir sind für einen sehr großen Teil des Umsatzes verantwortlich und sind in allen Projekten gefragt. 
              <br/> 
              <br/> Neben der Sicherung der finanziellen Stabilität liegt der strategische und primäre Fokus auf der Aus- und Weiterbildung von 
              <br/> Developers sowie die Definition und Entwicklung von Standards. Wir sind das Bindeglied zwischen Platforms & UX/UI und haben daher auch 
              <br/> den klaren Auftrag, die Verbindung in beide Richtungen zu intensivieren.
              <br/> 
              <br/> Wir müssen Standards für eine gemeinsame Zusammenarbeit definieren. 
              <br/> Ein anderer Fokus liegt auf der Verbesserung von Geschwindigkeit und Qualität sowie der Ramp-Up Zeit. 
              
              <br/> 

    </p>
  </div>
const linkName=readMore?'Weniger Anzeigen ':'Mehr Anzeigen '
return (

  
  <div className="box">
      <div className='meinkreis'>
            
            <h1 classname='überschrift' > Dein Kreis: Service Forge</h1>
            <br /> <span style={{fontWeight: 'bold'}}>Beschreibung:</span> 
              <br/> Unsere Service Forge ist der Ort an dem individuell zugeschnittene Lösung entstehen. 
              <br/> Natürlich setzen wir bei unserer Beratung auf viele Standards, die sich bewährt und eine gewisse Marktreife haben. 
              <br/> Das ist notwendig, um Geschwindigkeit aufnehmen und eine hohe Qualität sicherstellen zu können. 
              <br/> 
              <br/> 



    


  
    <a className="read-more-link" onClick={()=>{setReadMore(!readMore)}}><h2>{linkName}</h2></a>
    {readMore && extraContent}
  
    </div>
    </div>
);
}




export default Meinkreis