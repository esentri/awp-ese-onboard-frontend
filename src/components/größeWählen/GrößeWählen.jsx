import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {createTheme, ThemeProvider } from '@mui/material/styles';
import { setSize, getSize } from '../../Api/bestellungApiCall';
import { useEffect, useState } from "react";
import { getBenutzerId} from '../../pages/login/Login';

const theme = createTheme({
  palette: {
    Esentri: {
      main: '#99CC33'
    },

    Schwarz: {
      main: '#333333'
    }
  },
});


const GrößeWählen = () => {

  const [benutzerId, setBenutzerId] = useState(getBenutzerId());
  const [größe, setGröße] = React.useState('')


  const change = (event) => {
    setGröße(event.target.value)
    //Größe in DB speichern
    setSize(benutzerId, event.target.value)
  }

  return (
  <ThemeProvider theme={theme}>
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="Größe" color='Schwarz'>Größe</InputLabel>
        <Select
          labelId="größeWählen"
          id="größeWählen"
          value ={größe}
          label="Größe"
          color="Esentri"
          onChange={change}
        >
          <MenuItem value={"S"}>S</MenuItem>
          <MenuItem value={"M"}>M</MenuItem>
          <MenuItem value={"L"}>L</MenuItem>
          <MenuItem value={"XL"}>XL</MenuItem>
          <MenuItem value={"XXL"}>XXL</MenuItem>
        </Select>
      </FormControl>
    </Box>
  </ThemeProvider>
  )
}

export default GrößeWählen