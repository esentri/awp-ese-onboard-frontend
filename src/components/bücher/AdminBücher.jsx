import * as React from 'react';
import { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import buch1 from "../../resources/Clean_Code.png"
import buch2 from "../../resources/Clean_Architecture.png"
import { TextField } from '@mui/material';
import { getGoodieNameById, getGoodieCommentById, putGoodie } from '../../Api/goodieApiCall';


const AdminBücher = ({ type }) => {

  var goodieId;
  const [open, setOpen] = React.useState(false);
  const [name, setName] = useState([]);
  const [comment, setComment] = useState([]);
  
  //Name und Kommentar werden aus der DB geladen
  useEffect(() => {
    getGoodieNameById(goodieId)
    .then(data => {setName(data);
    }); 
  },[]);
  useEffect(() => {
    getGoodieCommentById(goodieId)
    .then(data => {setComment(data);
    }); 
  },[]);

  //Eingegebener Text wird in setName und setComment zwischengespeichert
  const handleChangeName = e =>  {
    setName(e.target.value)
  }
  const handleChangeComment = e =>  {
    setComment(e.target.value)
  }

  const handleClickOpen = () => {
    setOpen(true);
  };
  
  const handleClose = () => {
    setOpen(false);
  }
  const handleCloseSpeichern = () => {
    putGoodie(goodieId, name, comment)
    setOpen(false);
  };

  let data;

  switch(type){
    case "produkt1":
      data={
       img1: <img className='adminBücher1' onClick={handleClickOpen} src={buch1}  height="170px" />,
      };
      goodieId = 7
      break;

    case "produkt2":
      data={
        img1: <img className='adminBücher2' onClick={handleClickOpen} src={buch2} height="170px" />,

      };
      goodieId = 8
      break;
  

        default:
        break;
    }

    return (
      <div className='widgets'>
          <div className='AdminBücher'>
            <div className='bild' onClick={handleClickOpen}>{data.img1} </div>
          </div>
  
          <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bild ändern"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          Hier logik zum uploaden des Bildes einfügen
          </DialogContentText>
          <br/>
          <div>
            Name des Bildes:
          </div>
          <TextField 
            hiddenLabel
            id="filled-hidden-label-normal"
            defaultValue={name}
            fullWidth
            multiline
            onChange={handleChangeName}
          />
          <br/>
          <br/>
          <div>
            Kommentar:
          </div>
          <TextField 
            hiddenLabel
            id="filled-hidden-label-normal2"
            defaultValue={comment}
            fullWidth
            multiline
            onChange={handleChangeComment}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Abbrechen</Button>
          <Button onClick={handleCloseSpeichern}>
            Speichern
          </Button>
        </DialogActions>
      </Dialog>
      </div>
    );
}

export default AdminBücher