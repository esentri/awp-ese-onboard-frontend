import React from 'react'
import buch1 from "../../resources/Clean_Code.png"
import buch2 from "../../resources/Clean_Architecture.png"
import "./bücher.scss"
import { useEffect, useState } from "react";
import { getBenutzerId } from '../../pages/login/Login';
import {getGoodiesBestellungsIdByBenutzerAndKategorie} from '../../Api/bestellungApiCall'
import { putBestellungGoodies, getBestellungGoodiesLastId, deleteBestellung} from '../../Api/bestellungApiCall';
import { KATEGORIE } from '../../models/goodies';
import { CreateNewBestellung } from '../CreateNewBestellung';

const Bücher = ({ type }) => {

  const [benutzerId, setBenutzerId] = useState(getBenutzerId());
  const [bestellungGoodieId, setBestellungGoodieId] = useState([]);
    
  var goodiesId
  var schalter1 = false;
  var schalter2 = false;
  
  useEffect(() => {
    getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Fachbuch)
    .then(data => {setBestellungGoodieId(data);
    }); 
  },[bestellungGoodieId]);


  async function handleClick () {
    var bgid = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Fachbuch)
    if(type==="produkt1" && schalter1===false && schalter2===false){
      document.getElementsByClassName("bücher1")[0].style.opacity="1";
      document.getElementsByClassName("bücher2")[0].style.opacity="0.5";
      schalter1= true;

      if (bgid !== undefined) {
        putBestellungGoodies(bgid, goodiesId); // goodies Id über Case 
      }
      else {
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
      }
      
    }
    else if(type==="produkt1" && schalter1===true && schalter2===false){
      document.getElementsByClassName("bücher1")[0].style.opacity="0.5";
      document.getElementsByClassName("bücher2")[0].style.opacity="0.5";
      schalter1= false;
      deleteBestellung(bgid)
    }
    else if(type==="produkt2" && schalter2===false && schalter1===false){
      document.getElementsByClassName("bücher1")[0].style.opacity="0.5";
      document.getElementsByClassName("bücher2")[0].style.opacity="1";
      schalter2 = true;

      if (bgid !== undefined) {
        putBestellungGoodies(bgid, goodiesId); // goodies Id über Case 
      }
      else {
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
      }
    }
    else if(type==="produkt2" && schalter2===true && schalter1===true){
      document.getElementsByClassName("bücher1")[0].style.opacity="0.5";
      document.getElementsByClassName("bücher2")[0].style.opacity="0.5";
      schalter2 = false;
      deleteBestellung(bgid)
    }

  }

  let data;

  switch(type){
    case "produkt1":
      data={
       img1: <img className='bücher1' onClick={handleClick} src={buch1}  height="170px" />,
      };
      goodiesId = 7;
      break;

    case "produkt2":
      data={
        img1: <img className='bücher2' onClick={handleClick} src={buch2} height="170px" />,

      };
      goodiesId = 8;
      break;
  

        default:
        break;

        
  
   
    }
    return (
      <div className='widgets'>
          <div className='bücher'>
                <div className='bild'>{data.img1}</div>
          </div>
      </div>
  
    )
  }

export default Bücher