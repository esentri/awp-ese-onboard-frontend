import React from 'react'
import "./teammitglied2.scss"
import simon from "../../resources/simon.png"

const Teammitglied2 = () => {

  return (

    <div className='box'>
        
        <div className='teammitglied2'>

        <div className='tillbild'><img src={simon} alt="till" height="200px" /></div>

          <div className='tilltext'> <h1>„Vom Frontend bis in die Message-Queue: als Allrounder arbeite ich mich in jedes Thema ein.“</h1>
            <br /> <span style={{fontWeight: 'bold'}}>Bei esentri seit:</span> 2019
            <br/> <span style={{fontWeight: 'bold'}}>Hobbies:</span> Sushi, mein Hund Benny, die Wäsche machen (mein persönliches Yoga)
            <br/> <span style={{fontWeight: 'bold'}}>Was motiviert mich?:</span> Mr.Krabs.Moneten.gif
            <br/> <span style={{fontWeight: 'bold'}}>Was Du über mich wissen solltest:</span> Es sieht nur so aus, als würde ich schlafen. In Wahrheit meditiere ich. Versprochen.
            
            <p/> <span style={{fontWeight: 'bold'}}>Simon Leitl</span> 
            <br/>Consultant
            </div> 
        </div>
    </div>
  )
}

export default Teammitglied2