import React from 'react'
import {createTheme, ThemeProvider } from '@mui/material/styles';
import SendIcon from '@mui/icons-material/Send';
import Button from '@mui/material/Button';
import { sendBestellung, setBestelltById } from '../../Api/bestellungApiCall';
import { useState } from "react";
import { getBenutzerId} from '../../pages/login/Login';
import swal from 'sweetalert';

const theme = createTheme({
    palette: {
      Esentri: {
        main: '#99CC33'
      },
    },
  });


const SpeicherButton = () => {
  const [benutzerId, setBenutzerId] = useState(getBenutzerId());

  const handleClick = () => {
    //Email versenden
    sendBestellung()
    setBestelltById(benutzerId)
    swal("Deine Bestellung wurde erfolgreich versandt")
  }

  return (
    <div>
        <ThemeProvider theme={theme}>
            <Button color="Esentri" onClick={handleClick} variant="contained" endIcon={<SendIcon />} >
                Senden
            </Button>
        </ThemeProvider>
    </div>

  )
}

export default SpeicherButton