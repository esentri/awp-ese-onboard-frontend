import React from 'react'
import { useEffect, useState } from "react";
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import "./wunschbox.scss"
import { getWunschboxTextByBenutzerId, putWunschboxByBenutzerId } from '../../Api/bestellungApiCall';
import { getBenutzerId } from '../../pages/login/Login';

const Wunschbox = () => {

  const [benutzerId, setBenutzerId] = useState(getBenutzerId());
  const [wunschboxText, setWunschboxText] = useState([]);

  useEffect(() => {
    getWunschboxTextByBenutzerId(benutzerId)
    .then(data => {setWunschboxText(data);
    }); 
  },[]);


  const handleChange = e =>  {
    setWunschboxText(e.target.value)
    //Text wird direkti bei Eingabe in die DB gespeichert
    putWunschboxByBenutzerId(benutzerId, wunschboxText)
  }   

  return (
    <div className='Wunschbox'>
      <Stack
        
        sx={{
          width: '100ch',
        }}
        spacing={2}
        noValidate
        autoComplete="off"
      >
        <TextField
          variant="filled"
          hiddenLabel
          defaultValue={wunschboxText}
          fullWidth
          multiline
          rows={10}
          onChange={handleChange} />
      </Stack>
    </div>
  )
}

export default Wunschbox