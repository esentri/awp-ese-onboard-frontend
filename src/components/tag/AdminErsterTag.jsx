import React from 'react'
import Bild_1 from "../../resources/Bild_1.png"
import "./adminTag.scss"
import "./tag.scss"
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import swal from 'sweetalert';
import { useEffect, useState } from "react";
import {getErsterTagText, putErsterTagText} from '../../Api/ersterTagApiCall'





const AdminTag = () => {
  const [meinErsterTagText, setMeinErsterTagText] = useState([]);
    
  useEffect(() => {
    getErsterTagText()
    .then(data => {setMeinErsterTagText(data);
    }); 
  },[]);

  //Eingegebener Text wird in setCount zwischengespeichert
  const handleChange = e =>  {
    setMeinErsterTagText(e.target.value)
  }

  //Funktion, die aufgerufen wird, wenn Button geklickt wird
  //putErsterTagText wird aufgerufen
  function handleClick(){
    putErsterTagText(meinErsterTagText)
    //swal: schönerer Ersatz für "alert" von JavaScript
    //npm install --save sweetalert
    swal("Text wurde erfolgreich gespeichert")
  }

  return (

    <div className='box'>
     
        <div className='adminTag'>
            <h1> Mein erster Tag</h1>
            <TextField
              hiddenLabel
              id="filled-hidden-label-normal"
              defaultValue={meinErsterTagText}
              fullWidth
              multiline
              onChange={handleChange}
            />
            <Button variant="outlined" onClick={handleClick}>Speichern</Button>
            <img src={Bild_1} alt="Menschenkette" className="Menschenkette" />
            
        </div>
     
    </div>
  )
}

export default AdminTag