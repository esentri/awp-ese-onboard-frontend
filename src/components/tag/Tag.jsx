import React from 'react'
import Bild_1 from "../../resources/Bild_1.png"
import "./tag.scss"
import { Link } from "react-router-dom"
import {getErsterTagText} from "../../Api/ersterTagApiCall"
import { useState, useEffect } from 'react'

const Tag = () => {

  const [meinErsterTagText, setMeinErsterTagText] = useState([]);
    
  useEffect(() => {
    getErsterTagText()
    .then(data => {setMeinErsterTagText(data);
    }); 
  },[]);
  return (

    <div className='box'>
      <Link style={{textDecoration:"none"}} to="/tag">
        <div className='tag'>
            
            <h1> Mein erster Tag</h1>
            <div className='Absatz'> {meinErsterTagText}
            </div>
            <img src={Bild_1} alt="Menschenkette" className="Menschenkette" />
            
        </div>
        </Link>
    </div>
  )
}

export default Tag