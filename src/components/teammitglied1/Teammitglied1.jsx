import React from 'react'
import "./teammitglied1.scss"
import till from "../../resources/tillklaiber.png"

const Teammitglied1 = () => {

  return (

    <div className='box'>
        
        <div className='teammitglied1'>

        <div className='tillbild'><img src={till} alt="till" height="200px" /></div>

          <div className='tilltext'> <h1>"Kaffee redet nicht, Kaffee jammert nicht. Kaffee macht einfach seinen Job."</h1>
            <br /> <span style={{fontWeight: 'bold'}}>Bei esentri seit:</span> 2018
            <br/> <span style={{fontWeight: 'bold'}}>Hobbies:</span> Squash, Lesen, Death-Metal
            <br/> <span style={{fontWeight: 'bold'}}>Was motiviert mich?:</span> Der Kerl, den ich im Bildschirm sehe, wenn er aus ist.
            <br/> <span style={{fontWeight: 'bold'}}>Was Du über mich wissen solltest:</span> Es gibt kein Problem, zu dem es nicht auch eine Lösung gibt.
            Zusammen finden wir schon eine.
            <p/> <span style={{fontWeight: 'bold'}}>Till Klaiber</span> 
            <br/>Lead Service Forge
            </div> 
        </div>
    </div>
  )
}

export default Teammitglied1