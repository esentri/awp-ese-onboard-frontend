import * as React from 'react';
import { useEffect, useState } from "react";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import bildschirm17 from "../../resources/Bildschirm17.png"
import bildschirm15 from "../../resources/15gut.png"
import { TextField } from '@mui/material';
import { getGoodieNameById, getGoodieCommentById, putGoodie , putPictureByGoodieId} from '../../Api/goodieApiCall';

const AdminBildschirm = ({ type }) => {

  var goodieId;
  const [open, setOpen] = React.useState(false);
  const [name, setName] = useState([]);
  const [comment, setComment] = useState([]);
  
  //Name und Kommentar werden aus der DB geladen
  useEffect(() => {
    getGoodieNameById(goodieId)
    .then(data => {setName(data);
    }); 
  },[]);
  useEffect(() => {
    getGoodieCommentById(goodieId)
    .then(data => {setComment(data);
    }); 
  },[]);
  

  // getPicture by goodieId UseEffekt

  const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  var baseString;

  var proImage= new Image();
  proImage.src = baseString;
  document.body.appendChild(proImage);

  //Eingegebener Text wird in setName und setComment zwischengespeichert
  const handleChangeName = e =>  {
    setName(e.target.value)
  }
  const handleChangeComment = e =>  {
    setComment(e.target.value)
  }

  const handleClickOpen = () => {
    setOpen(true);
  };
  
  const handleClose = () => {
    setOpen(false);
  }
  const handleCloseSpeichern = () => {
    const uploadedFile = document.querySelector('#fileUpload').files[0];
    toBase64(uploadedFile)
    .then(res => {
      baseString = JSON.stringify(res);
      putPictureByGoodieId(baseString, goodieId)
   
    })
    .catch(err => {
      return err;
  })
    
    putGoodie(goodieId, name, comment)
    setOpen(false);
  };

  let data;
    switch(type){
        case "produkt1":
          data={
           img1: <img className='AdminBildschirm1' onClick={handleClickOpen} src={bildschirm15} alt="Logo" height="150px" />,
          };
          goodieId = 1
          break;
    
    
        case "produkt2":
          data={
            img1: <img className='AdminBildschirm2' onClick={handleClickOpen} src={bildschirm17} alt="plapla" height="150px" />,
          };
          goodieId = 2
          break;
    
          default:
          break;
      }

//zwei Textfelder
  return (
    <div className='widgets'>
        <div className='AdminBildschirm'>
          <div className='bild' onClick={handleClickOpen}>{data.img1} </div>
        </div>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Bild ändern"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <input type="file" id="fileUpload" name="fileUpload" />
           { //<button id="jsonConvert" onClick={handleClick} >Click me to upload file and convert into Base64</button>
           }
            
          </DialogContentText>
          <br/>
          <div>
            Name des Bildes:
          </div>
          <TextField 
            hiddenLabel
            id="filled-hidden-label-normal"
            defaultValue={name}
            fullWidth
            multiline
            onChange={handleChangeName}
          />
          <br/>
          <br/>
          <div>
            Kommentar:
          </div>
          <TextField 
            hiddenLabel
            id="filled-hidden-label-normal2"
            defaultValue={comment}
            fullWidth
            multiline
            onChange={handleChangeComment}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Abbrechen</Button>
          <Button onClick={handleCloseSpeichern}>Speichern</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AdminBildschirm