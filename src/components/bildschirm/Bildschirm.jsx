import React from 'react'
import bildschirm17 from "../../resources/Bildschirm17.png"
import bildschirm15 from "../../resources/15gut.png"
import { useEffect, useState } from "react";
import { putBestellungGoodies, getBestellungGoodiesLastId, deleteBestellung, getBestelltById} from '../../Api/bestellungApiCall';
import { KATEGORIE } from '../../models/goodies';
import { getBenutzerId} from '../../pages/login/Login';
import {getGoodiesBestellungsIdByBenutzerAndKategorie} from '../../Api/bestellungApiCall'
import { CreateNewBestellung } from '../CreateNewBestellung';
import "./bildschirm.scss"

const Bildschirm = ({ type }) => {
  
  const [benutzerId, setBenutzerId] = useState(getBenutzerId());
  const [bestellungGoodieId, setBestellungGoodieId] = useState([]);
    
  var goodiesId
  var schalter1 = false;
  var schalter2 = false;
  
  useEffect(() => {
    getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Laptop)
    .then(data => {setBestellungGoodieId(data);
    }); 
  },);

    //Hier Logik einfügen um Wahl in Datenbank zu speichern
    async function handleClick () {
      var bgid = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Laptop)
      console.log(bgid)
      if(type==="produkt1" && schalter1===false){
        //wähle Bildschirm1 aus, updaten oder hinzufügen
        document.getElementsByClassName("Bildschirm1")[0].style.opacity="1";
        document.getElementsByClassName("Bildschirm2")[0].style.opacity="0.5";
        schalter1= true;
        schalter2 =false;

        if(bgid !== undefined){ 
          putBestellungGoodies(bgid, goodiesId) // goodies Id über Case Laptop1 = Id 1 Laptop 2 = Id 2
        }
        else{
          await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
    
        }
      }
      else if(type==="produkt1" && schalter1===true){
        //Bildschirm 1 war bereits ausgewählt, soll gelöscht werden
        document.getElementsByClassName("Bildschirm1")[0].style.opacity="0.5";
        document.getElementsByClassName("Bildschirm2")[0].style.opacity="0.5";
        deleteBestellung(bgid)
        schalter1= false;
        schalter2= false;
      }
      else if(type==="produkt2" && schalter2===false){
        document.getElementsByClassName("Bildschirm1")[0].style.opacity="0.5";
        document.getElementsByClassName("Bildschirm2")[0].style.opacity="1";
        schalter2 = true;
        schalter1 = false;
        if(bgid !== undefined){ 
          putBestellungGoodies(bgid, goodiesId) // goodies Id über Case Laptop1 = Id 1 Laptop 2 = Id 2
        }
        else{
          await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
        }
      }
      else if(type==="produkt2" && schalter2===true){
        document.getElementsByClassName("Bildschirm1")[0].style.opacity="0.5";
        document.getElementsByClassName("Bildschirm2")[0].style.opacity="0.5";
        deleteBestellung(bgid)
        schalter2 = false;
      }
    }
  let data;
  switch(type){
      case "produkt1":
        data={
         img1: <img className='Bildschirm1' onClick={handleClick} src={bildschirm15} alt="Logo" height="150px" />,
        };
        goodiesId = 1
        break;
  
  
      case "produkt2":
        data={
          img1: <img className='Bildschirm2' onClick={handleClick} src={bildschirm17} alt="plapla" height="150px" />,
        };
        goodiesId = 2
        break;
        default:
        break;
  
   
    }
    return (
      <div className='widgets'>
          <div className='bildschirm'>
                <div className='bild'>{data.img1}</div>
          </div>
      </div>
  
    )
}



export default Bildschirm