import React from 'react'
import jabra from "../../resources/Jabra.png"
import airpods from "../../resources/AirPods.png"
import "./kopfhörer.scss"
import { useEffect, useState } from "react";
import { KATEGORIE } from '../../models/goodies';
import {getGoodiesBestellungsIdByBenutzerAndKategorie} from '../../Api/bestellungApiCall'
import { getBenutzerId } from '../../pages/login/Login';
import { CreateNewBestellung } from '../CreateNewBestellung';
import { putBestellungGoodies, getBestellungGoodiesLastId, deleteBestellung} from '../../Api/bestellungApiCall';


const Kopfhörer = ({ type }) => {

  var schalter1 = false;
  var schalter2 = false;
  var goodiesId
  const [benutzerId, setBenutzerId] = useState(getBenutzerId());
  const [bestellungGoodieId, setBestellungGoodieId] = useState([]);
    
  useEffect(() => {
    getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Kopfhörer)
    .then(data => {setBestellungGoodieId(data);
    }); 
  },);

  async function handleClick () {
    if(type==="produkt1" && schalter1===false){
      document.getElementsByClassName("kopfhörer1")[0].style.opacity="1";
      document.getElementsByClassName("kopfhörer2")[0].style.opacity="0.5";
      setBestellungGoodieId(await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Kopfhörer))
      var bgid = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Kopfhörer)
      
      if(bgid !== undefined){
        putBestellungGoodies(bgid, goodiesId) // goodies Id über Case 
      }
      else{
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
      }
      schalter1= true;
    }
    else if(type==="produkt1" && schalter1===true){

      document.getElementsByClassName("kopfhörer1")[0].style.opacity="0.5";
      document.getElementsByClassName("kopfhörer2")[0].style.opacity="0.5";
      schalter1= false;
      deleteBestellung(bestellungGoodieId)
    }
    else if(type==="produkt2" && schalter2===false){
      document.getElementsByClassName("kopfhörer1")[0].style.opacity="0.5";
      document.getElementsByClassName("kopfhörer2")[0].style.opacity="1";

    
        setBestellungGoodieId(await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Kopfhörer))
        var bgid = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Kopfhörer)
      
        if(bgid !== undefined){
          putBestellungGoodies(bgid, goodiesId) // goodies Id über Case 
        }
        else{
          await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
        }

      schalter2 = true;
    }
    else if(type==="produkt2" && schalter2===true){
      document.getElementsByClassName("kopfhörer1")[0].style.opacity="0.5";
      document.getElementsByClassName("kopfhörer2")[0].style.opacity="0.5";
      schalter2 = false;
      deleteBestellung(bestellungGoodieId)
    }
    
  }

    let data;
    switch(type){
        case "produkt1":
          data={
           img1: <img className='kopfhörer1' src={airpods} height="150px" onClick={handleClick} />,
          };
          goodiesId = 3
          break;
    
    
        case "produkt2":
          data={
            img1: <img className='kopfhörer2' src={jabra} height="150px" onClick={handleClick} />,
          };
          goodiesId = 4
          break;
    
          default:
          break;
    
     
      }
      return (
        <div className='widgets'>
            <div className='kopfhörer'>
                  <div className='bild'>{data.img1}</div>
            </div>
        </div>
    
      )
    }

export default Kopfhörer