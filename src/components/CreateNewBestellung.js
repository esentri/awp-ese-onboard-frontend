import { postBestellungGoodies, postBestellung, getBestellungGoodiesLastId} from '../Api/bestellungApiCall';

export async function CreateNewBestellung(benutzerId, goodiesId){
    await postBestellungGoodies(goodiesId); // goodies Id über Case
    var bgId = await getBestellungGoodiesLastId();
    await postBestellung(benutzerId, bgId);
}