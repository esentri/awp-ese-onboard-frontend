import React from 'react'
import "./teammitglied3.scss"
import anne from "../../resources/anneklaper.png"

const Teammitglied3 = () => {

  return (

    <div className='box'>
        
        <div className='teammitglied3'>

        <div className='tillbild'><img src={anne} alt="till" height="200px" /></div>

          <div className='tilltext'> <h1>„Offene und ehrliche Kommunikation im Projekt ist sehr wichtig.“</h1>
            <br /> <span style={{fontWeight: 'bold'}}>Bei esentri seit:</span> 2020
            <br/> <span style={{fontWeight: 'bold'}}>Hobbies:</span> Marvel, Paintball, ordentlich auf Twitter trollen
            <br/> <span style={{fontWeight: 'bold'}}>Was motiviert mich?:</span> Das freundschaftliche Verhältnis zu meinen Kollegen (auch nach Feierabend)
            <br/> <span style={{fontWeight: 'bold'}}>Was Du über mich wissen solltest:</span> Meine Brille hat eine Röntgen-Funktion.
            
            <p/> <span style={{fontWeight: 'bold'}}>Anne Klaper</span> 
            <br/>Consultant
            </div> 
        </div>
    </div>
  )
}

export default Teammitglied3