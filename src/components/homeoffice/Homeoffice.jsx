import React from 'react'
import {Box, FormControlLabel, Checkbox} from '@mui/material';
import { useEffect, useState } from 'react';
import {getGoodiesBestellungsIdByBenutzerAndKategorie} from '../../Api/bestellungApiCall'
import { getBenutzerId } from '../../pages/login/Login';
import { KATEGORIE } from '../../models/goodies';
import { CreateNewBestellung } from '../CreateNewBestellung';
import { getBestellungGoodiesLastId, deleteBestellung} from '../../Api/bestellungApiCall';

const Homeoffice = ({ type }) => {

  const [benutzerId, setBenutzerId] = useState(getBenutzerId());
  const [bestellungGoodieId, setBestellungGoodieId] = useState([]);
    
  var goodiesId
  const [accept1, setAccept1] = useState(false)
  const [accept2, setAccept2] = useState(false)
  const [accept3, setAccept3] = useState(false)
  const [accept4, setAccept4] = useState(false)
  const [accept5, setAccept5] = useState(false)

  //beim Neu Laden der Seite wird überprüft, ob es bereits eine Bestellung in der Kategorie Maus gibt
  useEffect(() => {
    getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Maus)
    .then(data => {setBestellungGoodieId(data);
    }); 
  }, [bestellungGoodieId]);
  
    //Maus
    async function handleChange1 (event) {
      //Problem: checkbox bleibt nicht angeglickt, wenn man gelöscht hat, Seite nicht lädt und gleich wieder bestellen will
      setAccept1(event.target.checked)
      var bgId = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Maus)
      goodiesId = 10
      if(bgId !== undefined){
        //wenn Bestellung bereits existiert und man drauf klickt, wird Bestellung gelöscht
        deleteBestellung(bgId)
      }
      else{
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
        //bestellungGoodieId wird nicht gesetzt, bleibt leer
      }
    }

    //Tastatur
    async function handleChange2(event) {
      setAccept2(event.target.checked)
      var bgId = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Tastatur)
      goodiesId = 11
      if(bgId !== undefined){
        //wenn Bestellung bereits existiert und man drauf klickt, wird Bestellung gelöscht
        deleteBestellung(bgId)
      }
      else{
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
      }
    }

    //Laptopständer
    async function handleChange3(event) {
      setAccept3(event.target.checked)
      var bgId = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Laptopständer)
      goodiesId = 12
      if(bgId !== undefined){
        //wenn Bestellung bereits existiert und man drauf klickt, wird Bestellung gelöscht
        deleteBestellung(bgId)
      }
      else{
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
      }
    }


    //Bildschirm
    async function handleChange4(event) {
      setAccept4(event.target.checked)
      var bgId = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Bildschirm)
      goodiesId = 13
      if(bgId !== undefined){
        //wenn Bestellung bereits existiert und man drauf klickt, wird Bestellung gelöscht
        deleteBestellung(bgId)
      }
      else{
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
      }
    }

    //Webcam
    async function handleChange5(event) {
      setAccept5(event.target.checked)
      var bgId = await getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, KATEGORIE.Webcam)
      goodiesId = 14
      if(bgId !== undefined){
        //wenn Bestellung bereits existiert und man drauf klickt, wird Bestellung gelöscht
        deleteBestellung(bgId)
      }
      else{
        await CreateNewBestellung(benutzerId, goodiesId).then(setBestellungGoodieId(await getBestellungGoodiesLastId()))
      }
    }

  return (
    <Box>
      <Box>
        <FormControlLabel
          label="Maus"
          control={<Checkbox checked = {accept1} onChange={handleChange1} //onClick={handleClick}
          />}
        />
      </Box>
      <Box>
        <FormControlLabel
          label="Tastatur"
          control={<Checkbox checked = {accept2} onChange={handleChange2} //onClick={handleClick}
          />}
        />
      </Box>
      <Box>
          <FormControlLabel
            label="Laptopständer"
            control={<Checkbox checked = {accept3} onChange={handleChange3} //onClick={handleClick}
            />}
          />
        </Box>
        <Box>
          <FormControlLabel
            label="Bildschirm"
            control={<Checkbox checked = {accept4} onChange={handleChange4} />}
          />
        </Box>
        <Box>
          <FormControlLabel
            label="Webcam"
            control={<Checkbox checked = {accept5} onChange={handleChange5}/>}
          />
        </Box>
    </Box>
  
  )
}


export default Homeoffice