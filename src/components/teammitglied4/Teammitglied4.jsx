import React from 'react'
import "./teammitglied4.scss"
import tobi from "../../resources/tobiasherb.png"

const Teammitglied4 = () => {

  return (

    <div className='box'>
        
        <div className='teammitglied4'>

        <div className='tillbild'><img src={tobi} alt="till" height="200px" /></div>

          <div className='tilltext'> <h1>„Mit wenigen Prinzipien kann man seinen Code übersichtlich und einheitlich gestalten.“</h1>
            <br /> <span style={{fontWeight: 'bold'}}>Bei esentri seit:</span> 2021
            <br/> <span style={{fontWeight: 'bold'}}>Hobbies:</span> Abstraktion, Abstraktion, Abstraktion
            <br/> <span style={{fontWeight: 'bold'}}>Was motiviert mich?:</span> Abstraktion, Abstraktion, Abstraktion
            <br/> <span style={{fontWeight: 'bold'}}>Was Du über mich wissen solltest:</span> Abstraktion, Abstraktion, Abstraktion
            
            <p/> <span style={{fontWeight: 'bold'}}>Tobias Herb</span> 
            <br/> Senior Consultant
            </div> 
        </div>
    </div>
  )
}

export default Teammitglied4