import React from 'react'
import Sidebar from '../../components/sidebar/Sidebar'
import Header from '../../components/header/Header'
import MeinKreis from '../../components/meinkreis/Meinkreis'
import Teammitglied1 from '../../components/teammitglied1/Teammitglied1'
import Teammitglied2 from '../../components/teammitglied2/Teammitglied2'

import Teammitglied3 from '../../components/teammitglied3/Teammitglied3'
import Teammitglied4 from '../../components/teammitglied4/Teammitglied4'
import "./kreis.scss"

const Kreis = () => {
  return (
    <div className="home">
    <Sidebar />
    <div className="kreis">
      <Header /> 


      <div className="kreisinhalt">
        <MeinKreis />
        <div className='tm1'>
          <Teammitglied1/>
          </div>
        <div className='tm2'>
          <Teammitglied2/>
          </div>
        <div className='tm3'>
          <Teammitglied3/>
          </div>
        <div className='tm4'>
        <Teammitglied4/>
        </div>
      </div>
      </div>
      </div>
  )
}

export default Kreis