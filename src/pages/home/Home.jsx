
import React from 'react'
import Sidebar from '../../components/sidebar/Sidebar'
import Header from '../../components/header/Header'
import Tag from '../../components/tag/Tag'
import Startpaket from '../../components/startpaket/Startpaket'
import "./home.scss"

const Home = () => {
  
    return (
    <div className="home">
      <Sidebar />
      <div className="homecontainer">
        <Header /> 
        <Tag />
        <div className='startpaketcontainer'>
          <h1>Mein Startpaket</h1>
          <div>Stelle Dir hier dein Startpaket zusammen</div>
          <div className='produktcontainer'>
            <Startpaket type="produkt1"/>
            <Startpaket type="produkt2"/>
            <Startpaket type="produkt3"/>
          </div>
        </div>
      </div>
      </div>
  )}


export default Home