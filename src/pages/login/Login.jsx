import React, { useState} from 'react'
import LoginForm from "../../components/loginform/LoginForm"
import "./login.scss"
import esentriLogo from "../../resources/esentri_grau-gruen.png"
import {BENUTZER_ID, USER_ROLE} from "../../models/user"


function Login() {
  const adminUser = {
      
    name: 'Robin Gscheidle',
    password: '123456'
  }
  const userUser = {
    name: 'Eva Fiserova',
    password: '123' 
  }

  const [user, setUser] = useState({name: "", email: ""});
  const [error, setError] = useState("");

  const Login = details => {
    if (details.name === adminUser.name && details.password === adminUser.password){
      setUser({
        name: details.name,
        email: details.email
      });
      getBenutzerId(3);
      getRole(0)
    }else if(details.name === userUser.name && details.password === userUser.password){
      setUser({
        name: details.name,
        email: details.email
      });
      getBenutzerId(1);
      getRole(1)
    }
     else {
      console.log("Details do not match!");
      setError("Passwort und Email stimmen nicht überein");
      }

  }


  const Logout = () => {
    setUser ({ name: "", email: ""});
  }

  return (
    <div className ="Profil">

      <div className='Top'>
        <img src={esentriLogo} alt="Logo" className="logo1" height="60px" />
         <hr />
       </div>

       <div className='Inhalt'>
      {(user.email != "") ? (
        <div className='welcome'>
         <h2> Willkommen, <span>{user.name}</span></h2>
         <button onClick={Logout}>Logout</button>
         </div>
    ) : (
      <LoginForm Login={Login} error={error}></LoginForm>
    )}
    </div>
    </div>
  );

}

export function getBenutzerId(id){
if(id === 3){
  return BENUTZER_ID.Admin;
}
else {
  return BENUTZER_ID.User;
}  
}

export function getRole(id){
if(id === 3){
  return USER_ROLE.Admin;
}
else {
  return USER_ROLE.User;
}  
}


export default Login