import React from 'react'
import Sidebar from '../../components/sidebar/Sidebar'
import Header from '../../components/header/Header'
import "./tag.scss"
import AdminTag from '../../components/tag/AdminErsterTag'
import Tag from '../../components/tag/Tag'
import { getRole } from '../../pages/login/Login'

const ErsterTag = () => {
  var role = getRole();

  if(role === "user"){
  return (
    <div className="home">
      <Sidebar />
      <div className="homecontainer">
        <Header /> 
        <Tag />
      </div>
    </div>
  )
  } else {
    return (
      <div className="home">
        <Sidebar />
        <div className="homecontainer">
          <Header /> 
          <AdminTag/>
        </div>
        </div>
    )
  }
}

export default ErsterTag