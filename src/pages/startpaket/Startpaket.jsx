import React from 'react'
import Sidebar from '../../components/sidebar/Sidebar'
import Header from '../../components/header/Header'
import "./startpaket.scss"
import Bildschirm from '../../components/bildschirm/Bildschirm'
import Kopfhörer from '../../components/kopfhörer/Kopfhörer'
import Kleider from '../../components/kleider/Kleider'
import Bücher from '../../components/bücher/Bücher'
import Homeoffice from '../../components/homeoffice/Homeoffice'
import Wunschbox from '../../components/wunschbox/Wunschbox'
import SpeicherButton from '../../components/speicherButton/SpeicherButton'
import GrößeWählen from '../../components/größeWählen/GrößeWählen'
import AdminBildschirm from '../../components/bildschirm/AdminBildschirm'
import AdminKopfhörer from '../../components/kopfhörer/AdminKopfhörer'
import AdminBücher from '../../components/bücher/AdminBücher'
import AdminKleider from '../../components/kleider/AdminKleider'
import { getBenutzerId, getRole } from '../../pages/login/Login';
import { useState } from "react";
import {bestellt} from '../../models/bestellt';

const Startpaket = () => {

  const [benutzerId, setBenutzerId] = useState(getBenutzerId());

  var role = getRole();

  if(role === "user"){

    //Abfrage, ob Kunde bereits Email versendet hat, wenn ja kommt eine swal Meldung
    bestellt(benutzerId)

    return (
      <div className="home">
        <Sidebar />
        <div className="homecontainer">
          <div className='Testcontainer'>
          <Header /> 
          <div className='bildschirmcontainer'>
            <p className='überschrift'>Welche Bildschirmgröße soll Dein MacBook haben?</p>
            <div className='produktcontainer'>
              <Bildschirm type="produkt1"/>
              <Bildschirm type="produkt2"/>
            </div>
          </div>
          <div className='kopfhörercontainer'>
            <p className='überschrift'>Wähle zwischen AirPods und dem Jabra Evolve aus!</p>
            <div className='produktcontainer'>
              <Kopfhörer type="produkt1"/>
              <Kopfhörer type="produkt2"/>
            </div>
          </div>
          <div className='kleidercontainer'>
          <p className='überschrift'>Welche Kleidergröße hast Du?</p>
            <div className='produktcontainer'>
              <GrößeWählen/>
              <Kleider type="produkt1"/>
              <Kleider type="produkt2"/>
            </div>
          </div>
          <div className='büchercontainer'>
          <p className='überschrift'>Wähle Dein erstes Fachbuch aus!</p>
            <div className='produktcontainer'>
              <Bücher type="produkt1"/>
              <Bücher type="produkt2"/>
            </div>
          </div>
          <div className='homeofficecontainer'>
          <p className='überschrift'>Wähle Deine Ausstattung für das Homeoffice aus! (Mehrfachauswahl möglich)</p>
            <Homeoffice />
          </div>
          <div className='wunschboxcontainer'>
            <div className='überschrift'>Das ist Deine Traumbox </div>
            <p/>Wie sieht Dein perfekter Arbeitsplatz aus?
            <br/>Was brauchst Du noch dafür?
            <br/>Wir können nichts versprechen, aber gerne schauen wir, was wir tun können.
            <Wunschbox />
          </div>
          <div className='speicherbuttoncontainer'>
            <SpeicherButton/>
          </div>
            
        </div>
      </div>
    </div>
    )
  }
  else{
    return (
      <div className="home">
        <Sidebar />
        <div className="homecontainer">
          <div className='Testcontainer'>
          <Header /> 
          <div className='bildschirmcontainer'>
            <p className='überschrift'>Welche Bildschirmgröße soll Dein MacBook haben?</p>
            <div className='produktcontainer'>
              <AdminBildschirm type="produkt1"/>
              <AdminBildschirm type="produkt2"/>
            </div>
          </div>
          <div className='kopfhörercontainer'>
            <p className='überschrift'>Wähle zwischen AirPods und dem Jabra Evolve aus!</p>
            <div className='produktcontainer'>
              <AdminKopfhörer type="produkt1"/>
              <AdminKopfhörer type="produkt2"/>
            </div>
          </div>
          <div className='kleidercontainer'>
          <p className='überschrift'>Welche Kleidergröße hast Du?</p>
            <div className='produktcontainer'>
              <GrößeWählen/>
              <AdminKleider type="produkt1"/>
              <AdminKleider type="produkt2"/>
            </div>
          </div>
          <div className='büchercontainer'>
          <p className='überschrift'>Wähle Dein erstes Fachbuch aus!</p>
            <div className='produktcontainer'>
              <AdminBücher type="produkt1"/>
              <AdminBücher type="produkt2"/>
            </div>
          </div>
          <div className='homeofficecontainer'>
          <p className='überschrift'>Wähle Deine Ausstattung für das Homeoffice aus! (Mehrfachauswahl möglich)</p>
            <Homeoffice />
          </div>
          <div className='wunschboxcontainer'>
            <div className='überschrift'>Das ist Deine Traumbox </div>
            <p/>Wie sieht Dein perfekter Arbeitsplatz aus?
            <br/>Was brauchst Du noch dafür?
            <br/>Wir können nichts versprechen, aber gerne schauen wir, was wir tun können.
            <Wunschbox />
          </div>
          <div className='speicherbuttoncontainer'>
            <SpeicherButton/>
          </div>
          </div>
        </div>
      </div>
    )
  }

}

export default Startpaket