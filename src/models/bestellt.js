import { getBestelltById } from "../Api/bestellungApiCall";
import swal from 'sweetalert';

export async function bestellt (benutzerId) {
    var i = await getBestelltById(benutzerId)
    if(i === "1"){
        swal("Du hast bereits eine Bestellung versendet, kontaktiere bitte bei Änderungswünsche diese Email: ")
    }
    return i
}