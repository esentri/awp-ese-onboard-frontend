export const KATEGORIE = {
    Laptop : "Laptop",
    Fachbuch: "Fachbuch",
    Kopfhörer: "Kopfhörer",
    Kleidung: "Kleidung",
    Maus: "Maus",
    Tastatur: "Tastatur",
    Laptopständer: "Laptopständer",
    Bildschirm: "Bildschirm",
    Webcam: "Webcam"
}