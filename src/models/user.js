import { Password } from "@mui/icons-material"

export const USER_ROLE = {
    Admin : "admin",
    User: "user"
}

export const BENUTZER_ID = {
    Admin : 3,
    User: 1
}


export const user = {
    id: 0,
    name: '',
    rolle: '',
    size: '',
    skills: '',
    kreisId: 0,
    bestellt: 0,
    passwort: ''
}