export async function getGoodieById(id){
    try{
        let response = await fetch (`http://localhost:8080/api/v1/goodies/${id}`, {
          method: 'GET',
          headers : {
            'Content-Type': 'application/json'
          }
        })
        if(!response.ok){
          response = null
          throw new Error('Gibt es nicht');
          
        } else {
          //resonse status ist 200
          response = response.json();
        }
        return response
      } catch (error){
        console.log(error);
      }
}

export async function getGoodieNameById(id){
    let response = await fetch (`http://localhost:8080/api/v1/goodies/name/${id}`)
    response = response.text()
    return response
}

export async function getGoodieCommentById(id){
    let response = await fetch (`http://localhost:8080/api/v1/goodies/comment/${id}`)
    response = response.text()
    return response
}

export async function putGoodie(id, name, comment){
    await fetch (`http://localhost:8080/api/v1/goodies/${id}`, {
      method: 'Put',
      headers : {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: name,
        comment: comment
      })
    }).then(res => {
      return res.json()
    })
}

export async function putPictureByGoodieId(baseString, id){
  await fetch (`http://localhost:8080/api/v1/goodiesPicture/${id}`, {
    method: 'Put',
    headers : {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      baseString: baseString
    })
  }).then(res => {
    return res.json()
  })
}

export async function getPictureByGoodieId(id){
  let response = await fetch (`http://localhost:8080/api/v1/goodiesPicture/${id}`)
  response = response.text()
  return response
}