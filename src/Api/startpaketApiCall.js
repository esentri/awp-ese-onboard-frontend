export async function getAllStartpakete() {
    let response = await fetch ("http://localhost:8080/api/v1/goodies/")
    response = response.json()
    return response
}


export async function getAllStartpaketeById(id) {
    let response = await fetch (`http://localhost:8080/api/v1/goodies/${id}`)
    response = response.json()
    return response
}

export async function getGoodiesByKategorie(kategorie) {
    let response = await fetch (`http://localhost:8080/api/v1/goodies/kategorie/${kategorie}`)
    response = response.json()
    return response
}

export async function getGoodiesIdByKategorie(kategorie) {
    let response = await fetch (`http://localhost:8080/api/v1/goodies/kategorie/id/${kategorie}`)
    response = response.json()
    return response
}


