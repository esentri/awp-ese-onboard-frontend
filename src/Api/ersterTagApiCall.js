
//mit /text/id bekommt man einen Text File und keinen JSON, deswegen .text() statt .json()
export async function getErsterTagText() {
  let response = await fetch ("http://localhost:8080/api/v1/ersterTag/text/1")
  response = response.text()
  return response
}
// updated den Text vom ersten Tag
export async function putErsterTagText(text){
  await fetch (`http://localhost:8080/api/v1/ersterTag/1`, {
    method: 'PUT',
    headers : {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id: 1,
      dashboardText: text
    })
  }).then(res => {
    return res.json()
  })
}
