export async function getGoodiesBestellungsIdByBenutzerAndKategorie(benutzerId, kategorie) {
  try{
    let response = await fetch (`http://localhost:8080/api/v1/bestellungGoodies/benutzer/${benutzerId}/${kategorie}`, {
      method: 'GET',
      headers : {
        'Content-Type': 'application/json'
      }
    })
    if(!response.ok){
      response = null
      throw new Error('Noch nicht angelegt');
      
    } else {
      //resonse status ist 200
      response = response.json();
    }
    return response
  } catch (error){
    console.log(error);
  }
}

export async function postBestellungGoodies(goodiesId){
    await fetch (`http://localhost:8080/api/v1/bestellungGoodies`, {
      method: 'POST',
      headers : {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          goodiesId: goodiesId
      })
    }).then(res => {
      return res.json()
    })
}

export async function getBestellungGoodiesLastId(){
  let response = await fetch('http://localhost:8080/api/v1/bestellungGoodies/lastId')
  response = response.text()
  return response
}

export async function putBestellungGoodies(id, goodiesId){
    await fetch (`http://localhost:8080/api/v1/bestellungGoodies/${id}`, {
      method: 'Put',
      headers : {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id,
        goodiesId: goodiesId
      })
    }).then(res => {
      return res.json()
    })
}

export async function postBestellung(benutzerId, bestellungGoodiesId){
  await fetch (`http://localhost:8080/api/v1/bestellung`, {
      method: 'POST',
      headers : {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          benutzerId : benutzerId,
          bestellt : true,
          bestellungGoodiesId: bestellungGoodiesId
      })
    }).then(res => {
      return res.json()
    })
}

export async function deleteBestellung(bestellungGoodiesId){
  await fetch (`http://localhost:8080/api/v1/bestellung/${bestellungGoodiesId}`, {
      method: 'DELETE',
      headers : {
        'Content-Type': 'application/json'
      }
    })
}

export async function getWunschboxTextByBenutzerId(benutzerId){
  let response = await fetch (`http://localhost:8080/api/v1/wunschbox/text/${benutzerId}`)
    response = response.text();
    //Wenn exeption Handling läuft -> respose.status rückgabe -> if(status == 200) Do This 
    return response
}

export async function putWunschboxByBenutzerId(benutzerId, text){
  await fetch (`http://localhost:8080/api/v1/wunschbox/text/1`, {
    method: 'Put',
    headers : {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      benutzer_id: benutzerId,
      text: text
    })
  }).then(res => {
    return res.json()
  })
}

export async function getSize(benutzerId){
  let response = await fetch (`http://localhost:8080/api/v1/benutzer/size/${benutzerId}`, {
    method: 'GET',
      headers : {
        'Content-Type': 'application/json'
      }
    })
  return response
}

export async function setSize(benutzerId, size){
  await fetch (`http://localhost:8080/api/v1/benutzer/size/${benutzerId}`, {
    method: 'Put',
    headers : {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      size: size
    })
  }).then(res => {
    return res.json()
  })
}

export async function sendBestellung(){
  let response = await fetch (`http://localhost:8080/api/v1/sendMail`, {
    method: 'GET',
      headers : {
        'Content-Type': 'application/json'
      }
    })
  return response
}

export async function getBestelltById(benutzerId){
  let response = await fetch(`http://localhost:8080/api/v1/benutzer/bestellt/${benutzerId}`)
  response = response.text()
  return response
}

export async function setBestelltById(benutzerId){
  await fetch (`http://localhost:8080/api/v1/benutzer/bestellt/1`, {
    method: 'Put',
    headers : {
      'Content-Type': 'application/json'
    }, body: JSON.stringify({
      bestellt: 1
    })
  }).then(res => {
    return res.json()
  })
}