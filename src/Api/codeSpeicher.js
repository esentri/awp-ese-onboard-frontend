import React from 'react'
import Sidebar from '../../components/sidebar/Sidebar'
import Header from '../../components/header/Header'
import "./kreis.scss"
import { useState } from 'react'



const Kreis = () => {

  const [picture, setPicture] = useState([]);

// getPicture by goodieId UseEffekt


  const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  var baseString;

  const handleClick = () => {
    const uploadedFile = document.querySelector('#fileUpload').files[0];
    toBase64(uploadedFile)
    .then(res => {
    setPicture(res);
    // if getPictureByGoodiesId false -> post else -> put
    // post picture or put
  })
  .catch(err => {
    return err;
  })
}

  var proImage= new Image();
  proImage.src = baseString;
  document.body.appendChild(proImage);


  return (
    <div className="home">
    <Sidebar />
    <div className="kreis">
      <Header />
      Kreis 
      <input type="file" id="fileUpload" name="fileUpload" />
      <button id="jsonConvert" onClick={handleClick} >Click me to upload file and convert into Base64</button>
      <img src={picture} alt="red dot" />
      
      </div>
      </div>
  )
}

export default Kreis